package com.gitlab.sf.projects.docserver;

import com.gitlab.sf.projects.docserver.DocServerProperties;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DocServerPropertiesTest {

    @Test
    public void testDocPath1() {
        DocServerProperties underTest = new DocServerProperties();
        underTest.setDocPath("/1/2/3");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }

    @Test
    public void testDocPath2() {
        DocServerProperties underTest = new DocServerProperties();
        underTest.setDocPath("/1/2/3/");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }

    @Test
    public void testDocPath3() {
        DocServerProperties underTest = new DocServerProperties();
        underTest.setDocPath("/1/2/3///");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }

    @Test
    public void testDocPath4() {
        DocServerProperties underTest = new DocServerProperties();
        underTest.setDocPath("/1//2/3");
        assertThat(underTest.getDocPath()).isEqualTo("/1/2/3/");
    }
}