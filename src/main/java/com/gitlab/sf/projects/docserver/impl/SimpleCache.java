package com.gitlab.sf.projects.docserver.impl;

import com.gitlab.sf.projects.docserver.DocServerProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Component
@Slf4j
class SimpleCache implements Cache {
    /**
     * simple concurrent map used to store group with key groupId
     */
    private final ConcurrentMap<String, Group> groupByIdMapping = new ConcurrentHashMap<>();
    private final DocServerProperties docServerProperties;

    SimpleCache(DocServerProperties docServerProperties) {this.docServerProperties = docServerProperties;}

    @Override
    public Group getGroup(String groupId) {
        return groupByIdMapping.get(groupId);
    }

    @Override
    public Collection<Group> getGroups() {
        return groupByIdMapping.values().stream().sorted(Group::compareTo).collect(Collectors.toList());
    }

    @ServiceActivator(inputChannel = "cacheInputChannel", outputChannel = "nullChannel")
    public Artifact addArtifact(ArtifactItemInfo artifactItemInfo) {
        final Group group = getGroup(artifactItemInfo);
        Artifact artifact = getArtifact(artifactItemInfo, group);
        artifact.addVersion(artifactItemInfo);
        return artifact;
    }

    private Artifact getArtifact(ArtifactItemInfo artifactItemInfo, Group group) {
        String artifactId = artifactItemInfo.getArtifactId();
        if(!group.hasArtifact(artifactId)) {
            group.addArtifact(new Artifact(group, artifactId));
        }
        return group.getArtifact(artifactId);
    }

    private Group getGroup(ArtifactItemInfo artifactItemInfo) {
        String groupId = artifactItemInfo.getGroupId();
        if(!groupByIdMapping.containsKey(groupId)) {
            groupByIdMapping.put(groupId, new Group(groupId));
        }
        return groupByIdMapping.get(groupId);
    }

    @Scheduled(fixedDelayString = "${docserver.cache-cleanup-delay:2000}")
    protected void cacheCleanup() {
        List<ArtifactVersion> toBeRemoved = groupByIdMapping.values().stream()
                .map(Group::getArtifacts)
                .flatMap(Collection::stream)
                .map(item -> item)
                .map(Artifact::getVersions)
                .flatMap(Collection::stream)
                .filter(this::hasExistingFile)
                .collect(Collectors.toList());
        toBeRemoved.forEach(this::cacheCleanup);
    }

    private void cacheCleanup(ArtifactVersion artifactVersion) {
        Group group = artifactVersion.getGroup();
        group.removeVersion(artifactVersion);

        if(group.getArtifacts().isEmpty()) {
            log.info("Remove artifact {}", artifactVersion);
            groupByIdMapping.remove(group.getGroupId());
        }
    }

    private boolean hasExistingFile(ArtifactVersion version) {
        String docPath = docServerProperties.getDocPath();
        String path = docPath + File.separator + version.getPath();
        log.debug("Check artifact file {} for version {}", path, version);

        return !Files.exists(Paths.get(path));
    }
}
