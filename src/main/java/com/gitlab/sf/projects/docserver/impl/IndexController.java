package com.gitlab.sf.projects.docserver.impl;

import com.gitlab.sf.projects.docserver.DocServerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;


/**
 * Controller that serves a list of artifacts with link to their documentation.
 *
 * @see DocServerProperties#docTitle
 * @see Cache#getGroups()
 */
@Controller
public class IndexController {
    private final String VIEW_NAME = "index";

    private final DocServerProperties docServerProperties;
    private final Cache cache;

    public IndexController(DocServerProperties docServerProperties, Cache cache) {
        this.docServerProperties = docServerProperties;
        this.cache = cache;
    }

    @RequestMapping("/")
    public String getDocIndex(Model model) {
        model.addAttribute("title", docServerProperties.getDocTitle());
        Collection<Group> groups = cache.getGroups();
        model.addAttribute("groups", groups);

        return VIEW_NAME;
    }

}
