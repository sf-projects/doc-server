package com.gitlab.sf.projects.docserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.templateresolver.FileTemplateResolver;


/**
 * Doc server application class based on spring boot.
 */
@SpringBootApplication
@EnableConfigurationProperties(DocServerProperties.class)
@EnableScheduling
public class DocServerApplication extends WebMvcConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(DocServerApplication.class, args).registerShutdownHook();
    }

    @Autowired
    private DocServerProperties docServerProperties;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(
                "file:" + docServerProperties.getDocPath());
    }

    /**
     * @return thymeleaf file template resolver used to access artifact documentation
     */
    @Bean
    public FileTemplateResolver documentationResolver() {
        FileTemplateResolver result = new FileTemplateResolver();
        result.setPrefix(docServerProperties.getDocPath());
        result.setSuffix(".html");
        result.setCacheable(false);
        return result;
    }
}
